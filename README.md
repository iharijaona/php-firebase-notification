## Firebase admin - PHP

Sample code, showing how to send a notification from PHP

## Requirements

* Service account credentials (in `firebase.json`)
* Device tokens

## How to run it

```bash
$ composer install
$ php index.php

```