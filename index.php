<?php

require __DIR__ . '/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\RawMessageFromArray;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;

// Registered device tokens
$deviceTokens = [];

$factory = (new Factory)->withServiceAccount('./firebase.json');
$messaging = $factory->createMessaging();

$message = new RawMessageFromArray([
    'android' => [
        'priority' => 'high',
        'notification' => [
            'channelId' => 'notification',
            'title' => 'Déménagement (PHP)',
            'body' => 'Une famille demande la permission de déménager dans votre fokontany',
            'sound' => "default",
            'visibility' => "public",
        ],
    ]
]);

// Validating Registration Tokens 
$result = $messaging->validateRegistrationTokens($deviceTokens);

// Send a message only if there are valid tokens
if (count($result['valid'])) {

    try {
        // Validating messages
        $messaging->sendMulticast($message, $result['valid'], $validateOnly = true);

        echo "\n-> Sending notification..." . PHP_EOL;
        $report = $messaging->sendMulticast($message, $result['valid']);
        echo "\tSuccessful sends: " . $report->successes()->count() . PHP_EOL;
        echo "\tFailed sends: " . $report->failures()->count() . PHP_EOL;

        // Handle errors on failure
        if ($report->hasFailures()) {
            foreach ($report->failures()->getItems() as $failure) {
                echo $failure->error()->getMessage() . PHP_EOL;
            }
        }

        echo "-> Done!" . PHP_EOL;
    } catch (InvalidMessage $e) {
        print_r($e->errors());
    }
}
// Create a pending notification if there is no valid token yet